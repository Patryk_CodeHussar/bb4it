import AccountCircle from "@mui/icons-material/AccountCircle";
import LockIcon from "@mui/icons-material/Lock";
import Button from "@mui/material/Button";
import InputAdornment from "@mui/material/InputAdornment";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import TextField from "@mui/material/TextField";
import React from "react";
import styled from "styled-components";
import bgimg from "../assets/bg-landing.png";
import swipeimg from "../assets/swipe_icon 1.png";
import TopBar from "./TopBar";
import chatimg from "../assets/chat_icon 1.png";
import partyimg from "../assets/party_icon 1.png";

const PageBG = styled.div`
  background-color: white;
`;

const BGimg = styled.img`
  position: absolute;
  z-index: -300;
  top: 0;
  left: 0;
  width: 100%;
  height: 50%;
`;

const Shader = styled.div`
  position: absolute;
  width: 100%;
  height: 50%;
  top: 0;
  left: 0;
  z-index: -10;
  background-color: rgba(0, 0, 0, 0.6);
`;

const Title = styled.h1`
  color: #ffffff;
  font-weight: bold;
  -webkit-text-stroke: 4px black;
  text-transform: uppercase;
  font-size: 3em;
  margin-top: 330px;
  margin-left: 80px;
`;

const LoginBox = styled.div`
  margin-left: auto;
  margin-right: 10%;
  background-color: rgba(0, 0, 0, 0.6);
  display: flex;
  flex-flow: column;
  padding: 10px;
  max-width: 300px;
`;

const LoginPageBox = styled.div`
  background-color: white;
  width: 80%;
  margin: auto;
  height: 50vh;
  box-shadow: 0px 4px 10px 10px rgba(0, 0, 0, 0.25);
  display:flex;
  flex-direction: column;
`;

const LoginTextFields = styled.div`
  display: flex;
  flex-flow: column;
`;

const TextFieldBackground = styled.div`
  background-color: white;
  display: inline-block;
  margin: 10px;
  border-radius: 50px;
`;

const BoxShade = styled.div`
  background: #f8f8f8;
  box-shadow: 0px 4px 10px 10px rgba(0, 0, 0, 0.25);
`;

const theme = createTheme({
  palette: {
    normal: {
      main: "#ffffffff",
      contrastText: "#fff",
    },
  },
});

const themeTextfield = createTheme({
  palette: {
    normal: {
      main: "none",
      contrastText: "#fff",
    },
  },
});

const StyledTextField = styled(TextField)`
  .MuiInputBase-root {
    background-color: white;
  }
`;

const ButtonStyling = styled.div`
  background-color: #ab0a00d9;
  margin: 10px;
  height: 55px;
  border-radius: 55px;
`;

const FigureStyling = styled.img`
  width: 150px;
  height: 150px;
`;

const FigureTextPositioning = styled.div`
  width: 30%;
  display: inline-flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  overflow-wrap: anywhere;
  margin: 10px;
`;

function PageImg(props) {
  return (
    <FigureTextPositioning>
      <FigureStyling src={props.src}></FigureStyling>
      <div style={{ fontSize: "25px" }}>{props.title}</div>
      <div style={{ fontSize: "12px" }}>{props.text}</div>
    </FigureTextPositioning>
  );
}

function logIn() {
  window.location.replace("/profile");
}

function Login() {
  return (
    <div>
      <TopBar />
      <BGimg src={bgimg} />
      <Shader />
      <LoginBox>
        <LoginTextFields>
          <TextFieldBackground>
            <ThemeProvider theme={themeTextfield}>
              <TextField
                variant="filled"
                label="Login"
                fullWidth="True"
                color="normal"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <AccountCircle />
                    </InputAdornment>
                  ),
                }}
              />
            </ThemeProvider>
          </TextFieldBackground>
          <TextFieldBackground>
            <ThemeProvider theme={themeTextfield}>
              <TextField
                variant="filled"
                label="Hasło"
                fullWidth="True"
                type="password"
                color="normal"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <LockIcon />
                    </InputAdornment>
                  ),
                }}
              />
            </ThemeProvider>
          </TextFieldBackground>
          <ThemeProvider theme={theme}>
            <ButtonStyling>
              <Button
                variant="text"
                color="normal"
                fullWidth="True"
                style={{ height: "55px" }}
                onClick={() => logIn()}
              >
                Zaloguj
              </Button>
            </ButtonStyling>
          </ThemeProvider>
        </LoginTextFields>
      </LoginBox>

      <LoginPageBox>
        <div style={{fontSize:"36px",padding:"10px"}}>JAK TO DZIAŁA?</div>
        <div style={{display:"flex", justifyContent:"center"}}>
        <PageImg
          src={swipeimg}
          title="POLUB WYDARZENIE"
          text="WYBIERZ WYDARZENIE KTÓRE CIĘ INTERESUJE"
        />

        <PageImg
          src={chatimg}
          title="POZNAJ LUDZI"
          text="APLIKACJA INTELIGENTNIE DOPASUJE CIĘ DO GRUPY"
        />

        <PageImg
          src={partyimg}
          title="BAW SIĘ WSPÓLNIE"
          text="PÓJDŹ NA WYDARZENIE Z NOWYMI PRZYJACIÓŁMI"
        />
        </div>
      </LoginPageBox>
    </div>
  );
}

export default Login;
