import React, { useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import styled from "styled-components";
import SectionTitle from "./atoms/SectionTitle";
import Likepng from "../assets/like.png";
import Dislikepng from "../assets/dislike.png";
import ProjectBox from "./ProjectBox";
import bgimg from "../assets/bg-swipe.png";

import prjData from "../data/projects.json";
import TopBar from "./TopBar";

let initialLeft = 750;

export default () => {
  const [BGstyles, chagneBG] = useState({
    1: { marginTop: -200, opacity: 0, zIndex: 0, left: initialLeft },
    2: { marginTop: 0, opacity: 0.2, zIndex: -20, left: initialLeft },
    3: { marginTop: 90, opacity: 1, zIndex: 20, left: initialLeft },
    4: { marginTop: 0, opacity: 0, zIndex: 20, left: 3000 },
  });

  const LikeHandler = (isGood) => {
    const calcStyles = (oldOne) => {
      if (oldOne.left !== initialLeft) {
        if (isGood) return { ...oldOne, left: -3000 };
        return { ...oldOne, left: 3000 };
      }
      return oldOne;
    };
    let newSytles = {
      1: calcStyles(BGstyles[2]),
      2: calcStyles(BGstyles[3]),
      3: calcStyles(BGstyles[4]),
      4: calcStyles(BGstyles[1]),
    };
    chagneBG(newSytles);
  };

  return (
    <>
      <TopBar />
      <BGimg src={bgimg} />
      <Shader />
      <Container>
        <Background styles={BGstyles[1]}>
          <Card data={prjData[0]} LikeHandler={LikeHandler} />
        </Background>

        <Background styles={BGstyles[2]}>
          <Card data={prjData[1]} LikeHandler={LikeHandler} />
        </Background>

        <Background styles={BGstyles[3]}>
          <Card data={prjData[2]} LikeHandler={LikeHandler} />
        </Background>

        <Background styles={BGstyles[4]}>
          <Card data={prjData[3]} LikeHandler={LikeHandler} />
        </Background>
      </Container>
    </>
  );
};

const Card = ({ data, LikeHandler }) => {
  return (
    <>
      <GreenBox />
      <CardBorder>
        <Container>
          <ProjectBox data={data}></ProjectBox>
          <Row>
            <Col></Col>
            <Col>
              <Row>
                <Col>
                  <Like
                    isLike={true}
                    onClick={() => LikeHandler(true)}
                    src={Likepng}
                  />
                </Col>
                <Col>
                  <Like
                  height={200}
                    isLike={false}
                    onClick={() => LikeHandler(false)}
                    src={Dislikepng}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </CardBorder>
    </>
  );
};

const BGimg = styled.img`
  position: absolute;
  z-index: -300;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

const Shader = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index:-20;
  background-color: rgba(0, 0, 0, 0.6);
`;

const Background = styled.div`
  transition: all 2s ease;
  /* transition: z-index 0s; */
  position: fixed;
  /* margin-left: 30vw; */
  margin-top: ${(props) => props.styles.marginTop}px;
  opacity: ${(props) => props.styles.opacity};
  z-index: ${(props) => props.styles.zIndex};
  left: ${(props) => props.styles.left}px;
`;

const CardBorder = styled.div`
  /* border:solid 5px #4C6C84; */
  /* border-top: 0px; */
  /* background-color: #fff; */
  /* width:40vw; */
  max-width: 400px;
  /* margin: auto; */
  /* width: 800px; */
  /* height: 600px; */
  /* margin:auto; */
  /* border-top: solid 20px #ACF2D3;  this looks wierd idk why */
`;
const GreenBox = styled.div`
  /* background-color: #acf2d3; */
  border-bottom: solid 2px #000;
  width: 100%;
  height: 30px;
`;

const Like = styled.img`
  position: fixed;
  /* border: solid 5px #4C6C84; */
  /* border-radius: 50%; */
  /* border-spacing:10px; */
  /* padding:30px; */

  z-index: 3000;
  margin-top: -50px;
  margin-left: ${(p) => (p.isLike ? "-200px" : "-24px")};
  height: 100px;
`;
