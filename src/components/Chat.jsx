import React, { useState, useEffect, useRef } from "react";
import { collection } from "firebase/firestore";
import { db } from "../init-firebase.js";
import { getDatabase, ref, set, child, get, onValue } from "firebase/database";
import Avatar from "@mui/material/Avatar";
import Stack from "@mui/material/Stack";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import styled from "styled-components";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import TopBar from "./TopBar.jsx";
import bgimg from "../assets/bg-landing.png";
import swipeimg from "../assets/swipe_icon 1.png"

const receiver = "Kuba";
const userId = "2113";
const img = "https://images.unsplash.com/photo-1552058544-f2b08422138a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=699&q=80";
const grupa = "grupa2";

const ChatBackground = styled.div`
  background-color: #afafaf;
  color: black;
  padding: 10px;
  border-radius: 10px;
  display: inline-block;
  vertical-align: middle;
  position: relative;

  white-space: nowrap;

  /* min-width: 100px; */

  /* flex:1; */

  > span {
    max-width: 550px;
    float: left;
    white-space: normal;
    overflow-wrap: break-word;
  }
`;

const ChatMargin = styled.div`
  margin: 15px;
  margin-top: 30px;
  width: 100%;
  flex-direction: ${(p) => (p.float == "right" ? "row-reverse" : null)};
  /* display: block; */
  display: flex;
  width: 100%;
`;

const ChatAvatar = styled.div`
  display: inline-block;
  vertical-align: middle;
  float: ${(p) => p.float};
  padding-right: ${(p) => p.paddingr};
  padding-left: ${(p) => p.paddingl};
`;

const ChatNickname = styled.div`
  color: gray;
  position: absolute;
  width: 100%;
  top: -25px;
  left: ${(p) => p.left};
  right: ${(p) => p.right};
`;

const TextFieldOptions = styled.div`
  background-color: white;
  display: inline-block;
  width: 100%;
  border-radius: 10px;
`;

const ButtonOptions = styled.div`
  background-color: #4d96ea;
  display: inline-block;
  float: right;
`;

function getCurrentTime(param) {
  var today = param;
  var date =
    today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
  var time =
    today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var dateTime = date + " " + time;
  return dateTime;
}

function generateMessageId() {
  return Date.now();
}

function addMessageToGroup(groupName, data) {
  const db = getDatabase();
  set(ref(db, groupName + "/" + generateMessageId()), {
    date: Date.now(),
    ...data,
  });
}

const Chat = () => {
  const [allMessages, setAllMessages] = useState([]);

  useEffect(() => {
    // console.log("useEffect");
    // addMessageToGroup("grupa2", {
    //   img: "img1",
    //   member: "memmm2",
    //   text: "text2",
    // });
    const ref2 = ref(db);
    onValue(ref2, (snapshot) => {
      const data = snapshot.val();
      // console.log(data);
      setAllMessages(data.grupa2);
      divRef.current.scrollIntoView({ behavior: "smooth", block: "end" });
    });
  }, []);

  const [textInput, setTextInputVal] = useState("");
  const divRef = useRef(null);

  const setTextInput = (e) => {
    if (e.code == "Enter") {
      handleChangeButton();
    } else {
      setTextInputVal(e.target.value);
    }
  };

  const handleChangeButton = () => {
    addMessageToGroup("grupa2", {
      date: Date.now(),
      img: img,
      member: receiver,
      text: textInput,
    });
    setTextInputVal("");
  };

  const listItems = Object.values(allMessages)
    .sort((a, b) => a.date > b.date)
    .map((objekt) => {
      // console.log(objekt);
      const flaga = objekt.member === receiver ? true : false;
      return (
        <ChatMargin
          float={flaga ? "left" : "right"}
          key={objekt.member + Math.random().toString()}>
          <ChatAvatar
            float={flaga ? "none" : "right"}
            paddingr={flaga ? "10px" : "none"}
            paddingl={flaga ? "none" : "10px"}>
            <Avatar src={objekt.img} />
          </ChatAvatar>
          <ChatBackground>
            <ChatNickname
              left={flaga ? "0" : "none"}
              right={flaga ? "none" : "0"}>
              {objekt.member}
            </ChatNickname>
            <span>{objekt.text}</span>
          </ChatBackground>
        </ChatMargin>
      );
    });

  return (
    <div>
      <TopBar />
      <BGimg src={bgimg} />
      <Shader />
      <div>Czat:</div>
      <ChatContainer>
        {listItems}
        <div ref={divRef}></div>
      </ChatContainer>
      <div style={{ maxWidth: "350px", margin: "auto", marginTop: "20px" }}>
        <TextFieldOptions>
          <TextField
            variant='filled'
            // label="Napisz wiadomość..."
            color='none'
            fullWidth={true}
            value={textInput}
            onKeyDown={setTextInput}
            onChange={setTextInput}
          />
        </TextFieldOptions>
        <ButtonOptions>
          <Button variant='Contained' onClick={handleChangeButton}>
            SEND!
          </Button>
        </ButtonOptions>
      </div>
      {/* {allMessages
        ? Object.values(allMessages).map((group) =>
            Object.values(group).map((message) => <h4>{message.text}</h4>)
          )
        : null} */}
    </div>
  );
};

export default Chat;

const ChatContainer = styled.div`
  overflow-y: scroll;
  overflow-x: hidden;
  padding: 30px;
  margin: auto;
  height: 700px;
  width: 1000px;
  display: block;
`;

const BGimg = styled.img`
  position: absolute;
  z-index: -300;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

const Shader = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: -10;
  background-color: rgba(0, 0, 0, 0.6);
`;
