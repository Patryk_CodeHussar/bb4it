import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import styled from "styled-components";

import Event1 from "../assets/event1.png";
import Event2 from "../assets/event2.png";
import Event3 from "../assets/event3.png";
import Event4 from "../assets/juliaromeo.jpg";


const eventsImages = [Event1, Event2, Event3, Event4];

export default ({ data }) => {
  return (
    <Box>
      <Row>
        <Title>{data.name}</Title>
        <img src={eventsImages[data.logo -1]} />
        <b>{data.date}</b>
        <ShortDescription>
          {data.desc}
        </ShortDescription>
      </Row>
    </Box>
  );
};

const Box = styled.div`
  background: #fafafa;
  /* box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25); */
  padding: 20px;

  /* padding-top: 20px; */
`;

const Title = styled.h1`
  color: white;
  font-size: 2em;
  text-transform: capitalize;
  font-weight: bold;
  background-color: rgba(0, 0, 0, 0.8);
  padding: 15px;
`;

const ShortDescription = styled.h1`
  font-family: Roboto;
  font-style: normal;
  font-weight: bold;
  font-size: 15px;
  line-height: 16px;
  color: #989898;
`;
