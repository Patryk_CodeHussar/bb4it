import React from "react";
import styled from "styled-components";
import bgimg from "../assets/bg-landing.png";
import TopBar from "./TopBar";

import events from "../data/projects.json";
import { Event } from "./AdminPanel";


const Welcome = () => {
  return (
    <div>
      <TopBar />
      <BGimg src={bgimg} />
      <Shader />
      <Title>
        Bielsko&nbsp;biała
        <br /> swipe your event
      </Title>
      <WhiteBG>
        <h1>Najbliższe wydarzenia</h1>
        {Object.values(events).map((currEvent) => {
          return <Event event={currEvent} />;
        })}
      </WhiteBG>
    </div>
  );
};

export default Welcome;

const BGimg = styled.img`
  position: absolute;
  z-index: -300;
  top: 0;
  left: 0;
  width: 100%;
  /* height: 100% ; */
`;

const Shader = styled.div`
  position: absolute;
  width: 100%;
  height: 780px;
  top: 0;
  left: 0;
  z-index: -10;
  background-color: rgba(0, 0, 0, 0.6);
`;

const Title = styled.h1`
  color: #ffffff;
  font-weight: bold;
  -webkit-text-stroke: 4px black;
  text-transform: uppercase;
  font-size: 3em;
  margin-top: 150px;
  margin-left: 80px;
  margin-bottom: 120px;
`;

const WhiteBG = styled.div`
  background-color: white;
  box-shadow: 0px 4px 10px 10px rgba(0, 0, 0, 0.25);

  max-width: 80%;
  margin: auto;
  margin-top: 30px;
  padding: 20px;
`;
