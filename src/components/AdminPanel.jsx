import React, { useState, useEffect, useRef } from "react";
import styled from "styled-components";
import bgimg from "../assets/admin-bg.png";
import Event1 from "../assets/event1.png";
import Event2 from "../assets/event2.png";
import Event3 from "../assets/event3.png";
import Event4 from "../assets/juliaromeo.jpg";

import TopBar from "./TopBar";
import { Row, Col } from "react-bootstrap";
import { db } from "../init-firebase.js";
import { getDatabase, ref, set, child, get, onValue } from "firebase/database";

const eventsImages = [Event1, Event2, Event3, Event4];

function generateMessageId() {
  return Date.now();
}


function addNewEvent(eventName, date, logo, desc, loc, price) {
  const db = getDatabase();
  set(ref(db, "events/" + generateMessageId() + "/"), {
    name: eventName,
    date: date,
    logo: logo,
    desc: desc,
    loc: loc,
    price: price
  });
}

const AdminPanel = () => {

  
  
  
  const [events, setEvents] = useState([]);

  useEffect(() => {
  //   addNewEvent("Letnie Brzmienia 2022","18.09.2022r. 20:00",1,"Letnie brzmienia czyli najlepsze koncerty z największymi polskimi gwiazdami. Już niedługo w naszym mieście! Wystąpią m.in: Jan Nowak, Adam Testowy i gwiazda wieczoru Janusz Januszowski","Powstańców Śląskich 3, Bielsko-Biała","20 PLN");
  // addNewEvent("Koncert z okazji zakończenia wakacji","18.09.2022r. 18:00",2,"Koncert z okazji zakończenia wakacji. Wystąpią m.in: Jan Nowak, Adam Testowy i gwiazda wieczoru Janusz Januszowski","Powstańców Śląskich 26, Bielsko-Biała","30 PLN");
  // addNewEvent("Impreza integracyjna na rozpoczęcie roku szkolnego","18.09.2022r. 18:00",3,"Jest to impreza organizowana dla wszystkich uczniów naszego miasta. Przyjdźcie i poznajcie się lepiej :). Wystąpią m.in: Jan Nowak, Adam Testowy i gwiazda wieczoru Janusz Januszowski","Cicha 33, Bielsko-Biała","ZA DARMO");
  // addNewEvent("Romeo i Julia Co nas kusi","15.09.2022r. 21:00",4,"Przedstawienie zielone. Wszystkie chwyty dozwolone. Zamiany postaci, kostiumów, tekstu, itp...   \nNa koniec zielony koncert zespołu SixMM. bilety, rezerwacje miejsc tel. 534 621 499","Galeria Bielska BWA - Willa Sixta ul. Adama Mickiewicza 24","40 PLN");
    const ref2 = ref(db);
    onValue(ref2, (snapshot) => {
      const data = snapshot.val();
      console.log("all data")
       console.log(data);
      setEvents(data.events);
      console.log(events)
    });
  }, []);

  return (
    <div>
        <TopBar pages={['Welcome', 'Mapa', 'Swipe', 'Admin', 'Analityka']} />
      <BGimg src={bgimg} />
      <Shader />
      <div style={{display:"flex", justifyContent:"center", flexDirection:"column", alignItems:"center"}}>
      <Title>
         Wydarzenia
      </Title>
      <EventsBox style={{display:"flex", flexDirection:"column", alignItems:"center"}}>
      
      <PopupForm/>
        {Object.values(events).map((currEvent) => {
         return(
          <Event event={currEvent}/>
         )
        })}

      </EventsBox>
      </div>
    </div>
  );
};

export default AdminPanel;

const BGimg = styled.img`
  position: absolute;
  z-index: -300;
  top: 0;
  left: 0;
  width: 100%;
  height: 80%;
`;

const Shader = styled.div`
  position: absolute;
  width: 100%;
  height: 80%;
  top: 0;
  left: 0;
  z-index: -10;
  background-color: rgba(0, 0, 0, 0.6);
`;

const Title = styled.h1`
  color: #ffffff;
  font-weight: bold;
  -webkit-text-stroke: 4px black;
  text-transform: uppercase;
  font-size: 3em;
  margin-top:25px;
`;

const EventsBox = styled.div`
  width:80vw;
  background-color: #ffffff;
  margin-top:5px;
`;


export const Event = ({img, event}) => {


  return (
    <>
    <BackBox>
        <Row>
          <Col md="4">
          <Image src={eventsImages[event.logo - 1]}></Image>
          </Col>
          <Col md="8" style={{display:"flex",  flexDirection:"column", alignItems:"center"}}>
          <EventTitle>{event.name}</EventTitle>
          <EventDate>{event.date}</EventDate>

          <EventDesc>{event.desc}</EventDesc>
          <LocText>{event.loc}</LocText>
          <PriceText>{event.price}</PriceText>

          </Col>
        </Row>
    </BackBox>
    </>
  );
}

const Image = styled.img`
width:300px;
height:240px;
`;

const BackBox = styled.div`
  border-top: none;
  height: 25%;
  width: 90%;
   box-shadow: 3px 3px 3px 3px rgba(0, 0, 0, 0.25); 
  border-radius: 10px;
  padding:20px;
  margin-bottom: 5px;
  margin-top:10px;
`;

const EventTitle = styled.text`
  color: #867070;
  text-transform: uppercase;
  font-size: 28px;
`;

const EventDate = styled.text`
  color: #867070;
  font-size: 19px;
`;

const EventDesc = styled.text`
margin-top:20px;
  color:  #867070;
  font-size: 20px;
`;

const LocText = styled.text`
  margin-top:20px;
  color:  #929292;
  font-size: 15px;
`;

const PriceText = styled.text`
  margin-top:20px;
  color:  #929292;
  font-size: 15px;
`;


const AddBtn = styled.button`
background-image: linear-gradient(to right, #e52d27 0%, #b31217  51%, #e52d27  100%);
margin: 10px;
padding: 15px 45px;
text-align: center;
text-transform: uppercase;
transition: 0.5s;
background-size: 200% auto;
color: white;            
box-shadow: 0 0 20px #eee;
border-radius: 30px;
display: block;


:hover{
  background-position: right center; /* change the direction of the change here */
  color: #fff;
  text-decoration: none;
}
`;

const PopupForm = () => {
  const [open, setOpen] = useState(false);
  const closeModal = () => setOpen(false);
  
  const [state, setState] = React.useState({
    name: "Letnie Brzmienia 2022",
  logo: 1,
  date: "18.09.2022r. 20:00",
  desc: "Letnie brzmienia czyli najlepsze koncerty z największymi polskimi gwiazdami. Już niedługo w naszym mieście! Wystąpią m.in: Jan Nowak, Adam Testowy i gwiazda wieczoru Janusz Januszowski",
  loc: "Powstańców Śląskich 3, Bielsko-Biała",
  price: "20 PLN"
  })


  function handleChange(evt) {
    const value = evt.target.value;
    setState({
      ...state,
      [evt.target.name]: value
    });
  }

  
  function saveToDatabase() {
    setOpen(o => !o);
    addNewEvent(state.name,state.date,2,state.desc,state.loc,state.price);
  }


  return (
    <div>

      <AddBtn onClick={() => setOpen(o => !o)} >Dodaj nowe [+]</AddBtn>
      {open ? 
      <>
      <BGimg2 src={bgimg} />
      <Shader2 />
      <Popup> 
        
        <div style={{display:"flex", justifyContent:"center", alignItems:"center", height:"100vh", flexDirection:"column"}}>
        <Title>
         <div>Dodaj nowe wydarzenie</div>
        </Title>
        <form >
        <Row>
          <label style={{color:"white"}}>
        Nazwa wydarzenia
       
      </label>
      <input
        style={{ height:"40px"}}
          type="text"
          name="name"
          value={state.name}
          onChange={handleChange}
        />

      </Row>
    <Row>
    <label style={{color:"white"}}>
        Data
       
      </label>
      <input
        style={{height:"40px"}}
          type="text"
          name="date"
          value={state.date}
          onChange={handleChange}
        />
    </Row>
    <Row>
    <label style={{color:"white"}}>
        Opis
        
      </label>
      <textarea style={{ height:"200px"}} name="desc" value={state.desc} onChange={handleChange} />
      </Row>
      <Row>
          <label style={{color:"white"}}>
        Lokalizacja
       
      </label>
      <input
        style={{ height:"40px"}}
          type="text"
          name="loc"
          value={state.loc}
          onChange={handleChange}
        />

      </Row>
      <Row>
          <label style={{color:"white"}}>
        Cena
       
      </label>
      <input
        style={{ height:"40px"}}
          type="text"
          name="price"
          value={state.price}
          onChange={handleChange}
        />

      </Row>
      <Row>
          <label style={{color:"white"}}>
        Zdjęcie
       
      </label>
      <input
        style={{ height:"40px"}}
          type="text"
          name="img"
          value={state.img}
          onChange={handleChange}
        />

      </Row>
    </form>
    <AddBtn onClick={() => saveToDatabase()} >Zapisz</AddBtn>
        </div>

      </Popup> 
      </>
      : null}
    </div>
  );
};

const Popup = styled.div`
position:fixed;
top: 0%;
left: 0%;
height:100%;
width:100%;
background-color: transparent;
z-index: 110;
`;


const BGimg2 = styled.img`
  position: absolute;
  z-index: 5;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

const Shader2 = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 10;
  background-color: rgba(0, 0, 0, 0.6);
`;