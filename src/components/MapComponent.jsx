import React from "react";

import { MapContainer, TileLayer, Popup, Marker } from "react-leaflet";
import Swipe from "./Swipe";
import TopBar from "./TopBar";
import eventsArr from "../data/projects.json";

import LikeButton from "../assets/group-green.png";
import InfoButton from "../assets/group-info.png";
import styled from "styled-components";

const position = [49.8051, 19.034];

const MapComponent = () => {
  let markers = eventsArr.map((singleEvent) => {
    return (
      <Marker position={singleEvent.pos} key={singleEvent.name}>
        <Popup maxWidth={300}>
          <MyPopup
            title={singleEvent.name}
            date={singleEvent.date}
            desc={singleEvent.desc}
          />
        </Popup>
      </Marker>
    );
  });

  console.log(markers);

  return (
    <div>
      <TopBar />
      <MapContainer
        style={{ width: "100vw", height: "93vh" }}
        center={[49.8135, 19.060]}
        zoom={15}
        scrollWheelZoom={false}>
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
        />
        {markers}
      </MapContainer>
    </div>
  );
};

export default MapComponent;

const MyPopup = ({ title, date, desc }) => {
  return (
    <>
      <div>
        <b>{title}</b>
        <br />
        {date}
        <p>{desc}</p>
        <LikeContainer>
          <img src={LikeButton} />
          <img src={InfoButton} style={{ float: "right" }} />
        </LikeContainer>
        {/* <button>Polub</button>
        <button>Info</button> */}
      </div>
    </>
  );
};

const LikeContainer = styled.div`
  > img {
    cursor: pointer;
  }
`;
