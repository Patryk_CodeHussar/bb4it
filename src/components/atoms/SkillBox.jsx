import React from 'react';
import styled from "styled-components";

export default (props) => {
  return (
    <Box>
        <Title>
            {props.skill}
        </Title>
    </Box>
  );
}

const Box = styled.span`

background: linear-gradient(180deg, #FF0000 0%, #9D0606 99.98%, #00C2FF 99.99%, rgba(255, 0, 0, 0) 100%);
    border-radius: 40px;
    padding:10px;
    padding-left:35px;
    padding-right: 35px; 
    margin: 15px;
    border: 1px solid black;
`

const Title = styled.h1`
    display:inline;
    text-align:center;
    font-size: 17px;
    font-family: Roboto;
    margin: auto;
    font-style: normal;
    color:white;
`