import React from "react";
import styled from "styled-components";
import { Row, Col } from "react-bootstrap";
import SkillBox from "./atoms/SkillBox";
import Pencil from "../assets/pencil.png";
import Fire from "../assets/fire_icon.png";
import Runner from "../assets/runner_icon.png";
import BronzeMedal from "../assets/bronze_medal.png";
import BBBack from "../assets/bg-landing.png";
import Photo from "../img/zdjecie1.png";
import userData from "../data/users.json";
import TopBar from "./TopBar";

export default () => {
  return (
    <>
    <TopBar/>
    <BGimg src={BBBack} />
    <Shader />
    <div style={{display:"flex", justifyContent:"center", flexWrap: "wrap"}}>
    <Row style={{marginTop:"50px"}}>
    <Profile user={userData[0]} />
    </Row>
    </div>
    </>
  );
};

const Avatar = styled.img`
  height:300px;
  width:300px;
`

const Box = styled.div`
  background: #fafafa;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  padding: 40px;
  padding-top: 20px;
`;
const SmallTittle = styled.h1`
  margin-top:20px;
  font-family: Roboto;
  font-style: normal;
  font-size: 20px;
  line-height: 29px;
  font-weight: bold;
  color: #7a7a7a;
`;

const BigTittle = styled.h1`
  font-family: "Roboto", sans-serif;
  font-weight: bold;
  font-size: 20px;
  line-height: 29px;
  margin-top: 15px;
  color: #4fb084;
   color: #7a7a7a;
`;

const NameAndSurname = styled.h1`
  font-family: Roboto;
  font-style: normal;
  font-weight: bold;
  font-size: 25px;
  line-height: 29px;
  
  color: #9D0606;
`;

const Description = styled.h1`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 16px;
  color: #989898;
`;

const GreenLine = styled.div`
  background-image: linear-gradient(180deg, #FF0000 0%, #9D0606 99.98%, #00C2FF 99.99%, rgba(255, 0, 0, 0) 100%);
  border-top: none;
  height: 99%;
   box-shadow: 9px 9px 9px rgba(0, 0, 0, 0.25); 
   padding: 40px; 
  padding-top: 40px;
  border-radius: 35px 35px 0 0;
  margin-bottom: 5px;
`;


const Profile = ({ user }) => {
  return (
    <>

    <GreenLine style={{width: "80vw"}}>
      <Box style={{display:"flex", justifyContent:"center"}}>
        <Row>
          <Col md="6">
            <Row>
              <Col md="6">
                <Avatar src={Photo} />
              </Col>
              <Col md="6">
                <SmallTittle>IMIĘ</SmallTittle>
                <NameAndSurname> {user.name}</NameAndSurname>

                <SmallTittle>NAZWISKO</SmallTittle>
                <NameAndSurname>{user.surname}</NameAndSurname>

                <div style={{display:"flex", justifyContent:"center", flexDirection:"column"}}>
                <BigTittle >TWOJE ODZNAKI</BigTittle>
                <Row>
                  <img src={Runner} style={{height:"47px", width:"56px"}}/>
                  <img src={Fire} style={{height:"40px", width:"56px"}}/>
                  <img src={BronzeMedal} style={{height:"46px", width:"56px"}}/>
                  </Row>

                </div>
              </Col>
            </Row>
          </Col>
          <Col md="6">
            <BigTittle style={{display:"flex", justifyContent:"center"}}>O MNIE</BigTittle>
            <Description>
              {user.aboutMe}
            </Description>
            <BigTittle style={{display:"flex", justifyContent:"center"}}>ZAINTERESOWANIA</BigTittle>
           <div style={{display:"flex", flexWrap:"wrap", justifyContent:"center"}}>
              <SkillBox skill="Sport" />
              <SkillBox skill="Muzyka" />
              <SkillBox skill="Plaża" />
              <SkillBox skill="Koncerty" />
              <SkillBox skill="Programowanie" />
              <SkillBox skill="Książki" />
              </div>
              <BigTittle style={{display:"flex", justifyContent:"center"}}>LICZEBNOŚĆ GRUP</BigTittle>
           <div style={{display:"flex", flexWrap:"wrap", justifyContent:"center"}}>
              <SkillBox skill="1-4" />
              <SkillBox skill="5-8" />
              <SkillBox skill="9+" />
              </div>
          </Col>
        </Row>
        <img src={Pencil} style={{width:"30px", height:"30px",  position:"relative"}}/>
      </Box>
    </GreenLine>
    </>
  );
}

const BGimg = styled.img`
  position: absolute;
  z-index: -300;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100%;
`;

const Shader = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: -10;
  background-color: rgba(0, 0, 0, 0.6);
`;

