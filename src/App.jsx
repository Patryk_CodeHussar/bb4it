import { useState } from "react";
import TopBar from "./components/TopBar";
import MapComponent from "./components/MapComponent";
import Chat from "./components/Chat";
import Login from "./components/Login";
import Welcome from "./components/Welcome";
import Swipe from "./components/Swipe";
import Profile from "./components/Profile";
import AdminPanel from "./components/AdminPanel";

import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";

const dupa = [
  { CreateDBy: "dua", text: "text", dawd: "text", dwdwaa: "text", ccc: "text" },
  { aa: 2, bbb: "ddd" },
];

function App() {
  const [count, setCount] = useState(0);

  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Welcome />} />
        <Route path='/welcome' element={<Welcome />} />
        <Route path='/analityka' element={<AdminPanel />} />

        <Route path='/login' element={<Login />} />
        <Route path='/profile' element={<Profile />} />
        <Route path='/map' element={<MapComponent />} />
        <Route path='/swipe' element={<Swipe />} />
        <Route path='/chat' element={<Chat />} />
        <Route path='/admin' element={<AdminPanel />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
