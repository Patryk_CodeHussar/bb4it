// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAZlZxEOO48l9pvfDurgUQNjcdbgnTySBs",
  authDomain: "bielskohack.firebaseapp.com",
  projectId: "bielskohack",
  storageBucket: "bielskohack.appspot.com",
  messagingSenderId: "569424969315",
  appId: "1:569424969315:web:6b257065370a194bbc4256",
  databaseURL: "https://bielskohack-default-rtdb.europe-west1.firebasedatabase.app"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();

export  {app};

export {db};