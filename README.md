# Witajcie!!! , koniecznie zapoznajcie się z treścią poniżej
Nasz projekt ma dwa główne cele. Po pierwsze ma aktywizować młodych ludzi, tak aby uczestniczyli w różnych wydarzeniach kulturalnych w mieście, a jednocześnie łącząc ich w grupy (w zależności od preferencji) pozwoli im poszerzać krąg znajomych. 
Miasto korzystające z naszej platformy nie tylko promuje swoje wydarzenia, ale także ma dostęp do narzędzia analitycznego. Dzięki któremu w przyszłości może lepiej planować wydarzenia co do kategorii, miejsca, ceny oraz terminu. Jedną z ważniejszych cech naszego projektu jest wykorzystanie darmowego silnika wyszukiwania Elasticsearch oraz Kibana, co znacząco zmniejsza koszt wdrożenia systemu.


# Mockup'y aplikacji webowej

Widok użytkownika:
https://www.figma.com/proto/VNEmzwrXtpAsoqAwcihEZH/BB4IT?node-id=0%3A1&scaling=scale-down&starting-point-node-id=1%3A3&show-proto-sidebar=1

Widok administratora:
https://www.figma.com/proto/VNEmzwrXtpAsoqAwcihEZH/BB4IT?node-id=17%3A4&scaling=scale-down&starting-point-node-id=17%3A4&show-proto-sidebar=1

# Mockup'y aplikacji mobilnej

https://www.figma.com/proto/nee3SAysfPq8UnGP1fm5n4/Aplikacja-mobilna-BB4IT?node-id=1%3A64&scaling=scale-down&page-id=0%3A1&starting-point-node-id=1%3A64

# Link do prezentacji

https://docs.google.com/presentation/d/1G6tvBAzwJf4KhYEr1S_O5nV80IECFJIi/edit?usp=sharing&ouid=113082025666910458824&rtpof=true&sd=true
